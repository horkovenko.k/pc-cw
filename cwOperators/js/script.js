//////////////// task 1
/*
let simpleNumber = prompt("Enter number!", 0);

if (simpleNumber % 2 === 0) {
    alert('Number is Even');
} else {
    alert('Number is Odd');
}

*/

//////////////// task 2
/*
let language = prompt('Choose language ru, en, ukr!', 'ru');

let monthRu = ['Январь ', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь ', 'Ноябрь', 'Декабрь'];
let monthUkr = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'];
let monthEn = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

switch (language) {
    case 'ru':
        alert(monthRu);
        break;
    case 'en':
        alert(monthEn);
        break;
    case 'ukr':
        alert(monthUkr);
        break;
    default:
        alert("Enter correct value!")
        break;
}
*/

///////////////////////// task 3
/*
let maxNumber = 0;

for (let i = 0; i < 3; i++) {
    let simpleNumber = prompt("Enter number!", 0);
    if (simpleNumber > maxNumber) {
        maxNumber = simpleNumber;
    }
}

alert(maxNumber);

*/

///////////////////////// task 4
////// Можно и через объекты но для данной задачи мап идеально подходит
/*
let staff = new Map();

staff.set("Bill", "Boss");
staff.set("Nick", "Boss Junior");
staff.set("Andrew", "Gogol Mogol");
staff.set("John", "Cleaner");



let name = prompt('Enter name: Bill, Nick, Andrew, John!', 'Bill');

switch (name) {
    case 'Bill':
        alert(staff.get(name));
        break;
    case 'Nick':
        alert(staff.get(name));
        break;
    case 'Andrew':
        alert(staff.get(name));
        break;
    case 'John':
        alert(staff.get(name));
        break;
    default:
        alert("Enter correct name!")
        break;
}


 */



///////////////////////////////////// task 5

let drink = prompt('Enter drink: coffee(2usd), capuccino(3usd), tea(1usd)!', 'coffee');
let money = +prompt('Pay please!', 0);

if (drink !== 'coffee' && drink !== 'capuccino' && drink !== 'tea') {
    alert("Choose drink correctly please!");
}

if (drink === 'coffee') {
    if (money === 2) {
        alert(`Ваш напиток ${drink}. Спасибо за точную сумму!`)
    } else if (money > 2) {
        alert(`Ваш напиток ${drink} и сдача ${money - 2}usd`)
    } else if (money < 2) {
        alert('Please pay more money!')
    }
} else if (drink === 'capuccino') {
    if (money === 3) {
        alert(`Ваш напиток ${drink}. Спасибо за точную сумму!`)
    } else if (money > 3) {
        alert(`Ваш напиток ${drink} и сдача ${money - 3}usd`)
    } else if (money < 3) {
        alert('Please pay more money!')
    }
} else if (drink === 'tea') {
    if (money === 1) {
        alert(`Ваш напиток ${drink}. Спасибо за точную сумму!`)
    } else if (money > 1) {
        alert(`Ваш напиток ${drink} и сдача ${money - 1}usd`)
    } else if (money < 1) {
        alert('Please pay more money!')
    }
}
